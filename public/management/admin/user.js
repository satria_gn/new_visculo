function createuser() {
    $("#modalcreate").modal('show');
}

function detailuser(id) {
    $.ajax({
        type: "GET",
        url: "/master/user/detail/" + id,
        success: function(data) {
            if (data.detail == null) {
                Swal.fire('Data User kosong')
            }else{
                var d = new Date(data.detail.created_at);
                var gabung  = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
                $("#modaldetail").modal('show');
                $("#form-detail input[name=name_modal]").val(data.detail.name);
                $("#form-detail input[name=email_modal]").val(data.detail.email);
                $("#form-detail input[name=role_modal]").val(data.detail.role);
                $("#form-detail input[name=create_date_modal]").val(gabung);
            }
        }
    });
}

function edituser(id) {
    $.ajax({
        type: "GET",
        url: "/master/user/edit/" + id,
        success: function(data) {
            if (data.detail == null) {
                Swal.fire('Data User kosong')
            }else{
                var roles = data.detail.role;
                $("#modaledit").modal('show');
                $('.select2').select2({
                    width: '100%',
                    dropdownParent: $("#pilihrole")
                });

                $('.role').val(roles).trigger('change');
                $("#form-edit input[name=name]").val(data.detail.name);
                $("#form-edit input[name=email]").val(data.detail.email);
                $("#form-edit input[name=create_date]").val(data.detail.created_at);
            }
        }
    });
}