<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::create([
            'name' => 'admin nih',
            'email' => 'admin@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('admin')
        ]);
        User::create([
            'name' => 'customer nih',
            'email' => 'customer@gmail.com',
            'role' => 'customer',
            'password' => bcrypt('customer')
        ]);
        User::create([
            'name' => 'Satria Girinanda',
            'email' => 'satria@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('satria')
        ]);
        User::create([
            'name' => 'Farel Fabian',
            'email' => 'farel@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('farel')
        ]);
    }
}
