<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', [App\Http\Controllers\Controller::class, 'home'])->name('home');
Route::get('/home', [App\Http\Controllers\Controller::class, 'home'])->name('home');
Route::get('/dashboardadmin', [App\Http\Controllers\Admin\DashboardController::class, 'dashboardadmin'])->name('dashboardadmin');

Route::prefix('master')->group(function () {
    Route::get('/user/index', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('user.index');
    Route::post('/user/store', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('user.store');
    Route::get('/user/detail/{id}', [App\Http\Controllers\Admin\UserController::class, 'detail'])->name('user.detail');
    Route::get('/user/edit/{id}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('user.edit');
});
