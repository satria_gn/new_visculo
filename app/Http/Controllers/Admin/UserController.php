<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DB::table('users')
            ->select('users.*')
            ->orderBy('users.id', 'DESC')
            ->get();

        if (request()->ajax()) {
            return DataTables::of($data)
                ->addColumn('action', function ($get) {
                    $button = '<a onclick="detailuser(' . $get->id . ')" title="Detail" class="btn btn-sm" style="color: white; background-color: #283b60;"><i class="fas fa-regular fa-eye"></i> Detail</a>  ';
                    $button .= '<a onclick="edituser(' . $get->id . ')" title="Edit" class="btn btn-sm" style="color: white; background-color: #a48b21;"><i class="fas fa-pencil-alt"></i> Edit </a>';
                    $button .= '<a onclick="deleteuser(' . $get->id . ')" title="Delete" class="btn btn-danger btn-sm btn-delete-' . $get->id . '" style="color: white; margin-left:2px; font-size: 1px;"><i class="fas fa-trash"></i> ssss</a>';
                    return $button;
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        Alert::info('Success Insert', 'Data berhasil disimpan');
        return view('admin.user.index', ['data' => $data]);
    }

    public function store(Request $request)
    {
        $saveuser = new User;
        $saveuser->name = $request->name;
        $saveuser->email = $request->email;
        $saveuser->role = $request->role;
        $saveuser->password = Hash::make($request->password);
        $saveuser->save();
        Alert::success('Success Insert', 'Data berhasil disimpan');
        return redirect('/master/user/index');
    }

    public function detail($id)
    {
        $detailsuer = User::where('id', $id)->first();

        return response()->json([
            'error' => false,
            'detail' => $detailsuer
        ], 200);
    }

    public function edit($id)
    {
        $detailsuer = User::where('id', $id)->first();

        return response()->json([
            'error' => false,
            'detail' => $detailsuer
        ], 200);
    }
}
