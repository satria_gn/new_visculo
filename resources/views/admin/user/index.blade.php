@extends('admin.layout.template')
@section('content')
@include('sweetalert::alert')
<section style="background-color: success;" class="bg-success py-5">
    <div class="container">
        <h2 class="col-md-12 text-white">Menu User</h2>
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a onclick="createuser()" class="btn btn-sm btn-secondary"><i class="fa-sharp fa-solid fa-marker"></i>&nbsp Create new user</a>
            </li>
        </ul>
        <div class="row align-items-center py-5">
            <div class="col-md-12 text-white">
                <table style="background-color: #dbdbdb; font-size: 16px;" id="tb_user" class="table table-striped" cellspacing="0" width="100%">
                    <thead style="background-color: #36473f; color: white;">
                        <tr>
                            <th style="width: 10px; text-align: center">No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Create Date</th>
                            <th style="width: 250px;text-align: center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@include('admin.user.modal-create')
@include('admin.user.modal-detail')
@include('admin.user.modal-edit')
<script src="{{ asset('management/admin/user.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            width: '100%',
            dropdownParent: $("#modalcreate")
        });

        $("#tb_user").DataTable({
            ajax: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true,
            columns: [{
                    data: 'DT_RowIndex',
                    'searchable': false,
                    class: 'text-center'
                },
                {
                    data: 'name',
                    name: 'name',
                    class: 'text-left'
                },
                {
                    data: 'email',
                    name: 'email',
                    class: 'text-left'
                },
                {
                    data: 'role',
                    name: 'role',
                    class: 'text-left'
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                    class: 'text-left'
                },
                {
                    data: 'action',
                    name: 'action',
                    class: 'text-center'
                },
            ]
        });
    });
</script>
@endsection