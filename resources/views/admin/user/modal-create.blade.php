<div class="modal fade" id="modalcreate" role="dialog" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="exampleModalCenterTitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal Create User</h5>
            </div>
            <form id="form-create" method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="col-form-label">Name:</label>
                        <input type="text" class="form-control" id="" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-form-label">Email:</label>
                        <input type="text" class="form-control" id="" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-form-label">Role:</label>
                        <select id="" name="role" class="select2">
                            <option value="admin">Admin</option>
                            <option value="customer">Customer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-form-label">Password:</label>
                        <input type="text" class="form-control" id="" name="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>