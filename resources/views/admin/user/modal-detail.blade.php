<div id="modaldetail" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal Detail User</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-detail" enctype="multipart/form-data">
                            @csrf
                            <table style="border: 0px;">
                                <tbody>
                                    <tr>
                                        <th>Name </th>
                                        <td style="width: 20px; text-align: center;">:</td>
                                        <td><input style="font-size: 65px; border: white; background-color: white;" type="text" class="form-control form-control-sm" id="name_modal" name="name_modal" readonly></td>
                                    </tr>
                                    <tr>
                                        <th>Email </th>
                                        <td style="width: 20px; text-align: center;">:</td>
                                        <td><input style="font-size: 65px; border: white; background-color: white;" type="text" class="form-control form-control-sm" id="email_modal" name="email_modal" readonly></td>
                                    </tr>
                                    <tr>
                                        <th>Role </th>
                                        <td style="width: 20px; text-align: center;">:</td>
                                        <td><input style="font-size: 65px; border: white; background-color: white;" type="text" class="form-control form-control-sm" id="role_modal" name="role_modal" readonly></td>
                                    </tr>
                                    <tr>
                                        <th>Create Date</th>
                                        <td style="width: 20px; text-align: center;">:</td>
                                        <td><input style="font-size: 65px; border: white; background-color: white;" type="text" class="form-control form-control-sm" id="create_date_modal" name="create_date_modal" readonly></td>
                                        <!-- <td>
                                            <span id="status_modal" name="status_modal" class="badge badge-pill badge-success">Active</span>
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>