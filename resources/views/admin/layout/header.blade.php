<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .dropbtn {
            background-color: #fff;
            color: black;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #ddd;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: #3e8e41;
        }
    </style>
</head>
<nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
    <div class="container text-light">
        <div class="w-100 d-flex justify-content-between">
            <div>
                <i class="fa fa-envelope mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:info@company.com">info@visculo.com</a>
                <i class="fa fa-phone mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="tel:010-020-0340">010-020-0340</a>
            </div>
            <div>
                @if (Auth::check())
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
                <div class="menu-item">
                    <a style="font-size: 23px;" href="#" class="menu-link" onclick="lout()">Sign Out</a>
                </div>
                @else
                <a class="text-light" href="{{ route('login') }}"><i class="fab fa fa-user fa-sm fa-fw me-2"></i>Login</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="text-light" href="{{ route('register') }}"><i class="fab fa fa-address-card fa-sm fa-fw me-2"></i>Register</a>
                @endif
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">
        <a class="navbar-brand text-success logo h1 align-self-center" href="{{url('/')}}">
            Visculo
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <div style="margin-left: 80px;" class="dropdown">
                    <button class="dropbtn {{ request()->is('master*') ? 'dropbtn active' : '' }}">Master Data</button>
                    <div class="dropdown-content">
                        <a class="nav-link {{ request()->is('master/user/index') ? 'nav-link active' : '' }}" href="{{url('/master/user/index')}}">User</a>
                        <a href="#">Product</a>
                        <a href="#">Link 3</a>
                    </div>
                </div>
                <div style="margin-left: 80px;" class="dropdown">
                    <button class="dropbtn">Transaction</button>
                    <div class="dropdown-content">
                        <a href="#">Link 1</a>
                        <a href="#">Link 2</a>
                        <a href="#">Link 3</a>
                    </div>
                </div>
                <div style="margin-left: 80px;" class="dropdown">
                    <button class="dropbtn">Reporting</button>
                    <div class="dropdown-content">
                        <a href="#">Link 1</a>
                        <a href="#">Link 2</a>
                        <a href="#">Link 3</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>


</html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function lout() {
        Swal.fire({
            title: 'Anda telah logout',
            text: 'Beberapa fitur tidak bisa diakses',
            confirmButtonText: 'Ok'
        }).then((result) => {
            document.getElementById('logout-form').submit();
        })
    }
</script>