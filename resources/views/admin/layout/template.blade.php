<!DOCTYPE html>
<html lang="en">

<head>
    <title>Visculo - Optical Supply</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{asset('template')}}/img/apple-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/img/favicon.ico">
    <link rel="stylesheet" href="{{asset('template')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/css/templatemo.css">
    <link rel="stylesheet" href="{{asset('template')}}/css/custom.css">
    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{asset('template')}}/css/fontawesome.min.css">
    <!-- {{-- datatable --}} -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.3.1/css/rowReorder.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.4.0/css/responsive.dataTables.min.css" />
    <!-- select2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <style>
        a.active {
            background-color: green;
            color: white;
        }

        button.active {
            background-color: green;
            color: white;
        }
    </style>
</head>

<body>
    <!-- Header -->
    @include('admin.layout.header')
    <!-- Close Header -->
    <!-- content -->
    @yield('content')
    <!-- Start Footer -->
    @include('admin.layout.footer')
    <!-- End Footer -->
    <!-- Start Script -->
    <script src="{{asset('template')}}/js/jquery-1.11.0.min.js"></script>
    <script src="{{asset('template')}}/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{asset('template')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('template')}}/js/templatemo.js"></script>
    <script src="{{asset('template')}}/js/custom.js"></script>
    <!-- sweetalert2 -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- {{-- datatable --}} -->
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.3.1/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>
    <!-- font awesome icon -->
    <script src="https://kit.fontawesome.com/1e68e124a4.js" crossorigin="anonymous"></script>
    <!-- select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script type="text/javascript">

    </script>
</body>

</html>