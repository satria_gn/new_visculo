<nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
    <div class="container text-light">
        <div class="w-100 d-flex justify-content-between">
            <div>
                <i class="fa fa-envelope mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:info@company.com">info@visculo.com</a>
                <i class="fa fa-phone mx-2"></i>
                <a class="navbar-sm-brand text-light text-decoration-none" href="tel:010-020-0340">010-020-0340</a>
            </div>
            <div>
                @if (Auth::check())
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
                <div class="menu-item">
                    <a style="font-size: 23px;" href="#" class="menu-link" onclick="lout()">Sign Out</a>
                </div>
                @else
                <a class="text-light" href="{{ route('login') }}"><i class="fab fa fa-user fa-sm fa-fw me-2"></i>Login</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="text-light" href="{{ route('register') }}"><i class="fab fa fa-address-card fa-sm fa-fw me-2"></i>Register</a>
                @endif
            </div>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-light shadow">
    <div class="container d-flex justify-content-between align-items-center">
        <a class="navbar-brand text-success logo h1 align-self-center" href="{{url('/')}}">
            Visculo
        </a>

        <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
            <div class="flex-fill">
                <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('a') ? 'nav-link active' : '' }}" href="{{url('/home')}}">Lens</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('b') ? 'nav-link active' : '' }}" href="{{url('/home')}}">Eyewear</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('c') ? 'nav-link active' : '' }}" href="{{url('/home')}}">Contact Lens</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('d') ? 'nav-link active' : '' }}" href="{{url('/home')}}">Solution</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('e') ? 'nav-link active' : '' }}" href="{{url('/home')}}">Equipment</a>
                    </li>
                    @if (Auth::check())
                    @if ( auth()->user()->email == "admin@gmail.com")
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/dashboardadmin')}}">Admin</a>
                    </li>
                    @endif
                    @endif
                </ul>
            </div>
            <div class=" navbar align-self-center d-flex">
                <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputMobileSearch" placeholder="Search ...">
                        <div class="input-group-text">
                            <i class="fa fa-fw fa-search"></i>
                        </div>
                    </div>
                </div>
                <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                    <i class="fa fa-fw fa-search text-dark mr-2"></i>
                </a>
                <a class="nav-icon position-relative text-decoration-none" href="#">
                    <i class="fa fa-fw fa-cart-arrow-down text-dark mr-1"></i>
                    <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">7</span>
                </a>
                <a class="nav-icon position-relative text-decoration-none" href="#">
                    <i class="fa fa-fw fa-user text-dark mr-3"></i>
                    <span class="position-absolute top-0 left-100 translate-middle badge rounded-pill bg-light text-dark">+99</span>
                </a>
            </div>
        </div>

    </div>
</nav>
<div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="w-100 pt-1 mb-5 text-right">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="" method="get" class="modal-content modal-body border-0 p-0">
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-search text-white"></i>
                </button>
            </div>
        </form>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function lout() {
        Swal.fire({
            title: 'Anda telah logout',
            text: 'Beberapa fitur tidak bisa diakses',
            confirmButtonText: 'Ok'
        }).then((result) => {
            document.getElementById('logout-form').submit();
        })
    }
</script>