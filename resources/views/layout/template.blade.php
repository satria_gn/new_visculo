<!DOCTYPE html>
<html lang="en">

<head>
    <title>Visculo - Optical Supply</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="{{asset('template')}}/img/apple-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('template')}}/img/favicon.ico">

    <link rel="stylesheet" href="{{asset('template')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('template')}}/css/templatemo.css">
    <link rel="stylesheet" href="{{asset('template')}}/css/custom.css">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="{{asset('template')}}/css/fontawesome.min.css">
    <!--
    
TemplateMo 559 Zay Shop

https://templatemo.com/tm-559-zay-shop

-->
    <style>
        a.active {
            background-color: green;
            color: red;
        }
    </style>
</head>

<body>
    <!-- Start Top Nav -->

    <!-- Close Top Nav -->

    @include('sweetalert::alert')
    <!-- Header -->
    @include('layout.header')
    <!-- Close Header -->

    <!-- Modal -->



    <!-- content -->
    <!-- Start Banner Hero -->
    @yield('content')
    <!-- End Featured Product -->


    <!-- Start Footer -->
    @include('layout.footer')
    <!-- End Footer -->
    <!-- Start Script -->
    <script src="{{asset('template')}}/js/jquery-1.11.0.min.js"></script>
    <script src="{{asset('template')}}/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{asset('template')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('template')}}/js/templatemo.js"></script>
    <script src="{{asset('template')}}/js/custom.js"></script>
    <!-- End Script -->
    <!-- sweetalert2 -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</body>

</html>